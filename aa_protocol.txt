#define AVG_TEST				0xba01	//testing the averaging algorithm\n");
#define RAM_READ				0xba02	//general purpose read from SDRAM\n");


#define TCP_TEST				0xaa01	//benchmark test for TCP/IP transfer times

#define	IBIS4A_SYS_INFO				0xaa20	// get general system info
#define IBIS4A_TCP_GETCFG			0xaa21	// get configuration
#define IBIS4A_TCP_SETCFG			0xaa22	// set configuration
#define IBIS4A_TCP_GETEXPTIME			0xaa23	// get integration (exposure) time
#define IBIS4A_TCP_SETEXPTIME			0xaa24	// set integration (exposure) time
//#define IBIS4A_TCP_COLOURBITMAP		0xaa25	// get colour bitmap image											//deprecated
#define	READ_CURRENT_QUAD_CONFIG		0xaa25	//read current Quad configuration
#define IBIS4A_TCP_AVERAGE			0xaa26	// get average pixel value											//deprecated
#define CAPTURE_IMG_32BIT			0xaa2f	//capture 32 bit image Now obsolete following ability to sum with updates
#define CAPTURE_IMG_16BIT			0xaa30	//capture 16 bit image
#define CAPTURE_IMG_08BIT			0xaa31	//capture 8 bit image
#define CAPTURE_IMG_SGL_DARK			0xaa32	//single dark image acquisition. Prolly redundant after sum capability....
#define SUM_IMAGE				0xaa33	//sum a 32 bit image then transfer full 32 bits (mode=1) or 16 bits (mode=0). Updates will be transmitted.
#define SGL_PROFILE				0xaa34	//single shot profile Prolly obsolete now that continuous profile can be run once.
#define READ_IMG				0xaa35	//read an image already in the RAM.
#define SUM_IMG_WITH_PROF			0xaa36	//Summing 32 bit images, averaging and passing the profile
#define CONT_PROFILE				0xaa37	//continuous profile grab mode.  Skip as few frames as possible
#define SAVE_IMAGE				0xaa38	//receiving RAW image to store into SDRAM

/* averaging by the processor is disabled on
 * uClinux platform due to memory buffer allocation
 * specific which is not addressed by capture_16_bit_image_and_accum() */
#define CAPTURE_IMG_AVG_DARK			0xaa39	//Take averaged dark image

//DAC commands (control a AD56x7R/AD56x7 DAC which resides on the I2C-bus)
#define FF_DAC_SET_VOLTAGE			0xaa40 //set voltage value using a binary code

/* Accelerometer polling command (control a LIS3LV02DL MEMS inertial sensor
 * which resides on the I2C-bus) */
#define EVS_PTC					0xaa41	// acquire photon transfer curve (PTC) data
#define EVS_TEMPERATURE				0xaa42	/* get temperature readings
										(from TI TMP42x sensor) */
#define EVS_SHUTTER_MOVE			0xaa43	/* move image sensor shutter */
#define EVS_SHUTTER_STATUS			0xaa44	/* read status of the imager shutter */
#define EVS_PELTIER_READ			0xaa45	/* read current Peltier parameters */
#define EVS_PELTIER_WRITE			0xaa46	/* set Peltier parameters */
#define EVS_ADC_READ				0xaa47	/* read ADC channels */
#define EVS_ADC_SCALE_SET			0xaa48	/* set ADC scale */
#define FF_ACC_GET_ACCELERATION 		0xaa50 //get acceleration data


#define FF_I2C_READ				0xcc00 //read i2c device register
#define FF_I2C_WRITE				0xcc01 //write to the i2c deivce register

