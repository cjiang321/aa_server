/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NETCOMMAND_H
#define NETCOMMAND_H

#include <map>
#include <string>
#include <utility>

#include "EVSystem.h"
#include "NetData.h"

namespace EVS {

/**
 * @todo write docs
 */
class NetCommand : std::enable_shared_from_this<NetCommand> {
public:
    /**
     * represents one network command
     * @param read number of bytes to read before output
     * @param write number of bytes to output, only counts fixed payload.
     * note the size of image/profile data is not included as they are variable
     */
    explicit NetCommand(std::string desc, size_t read, size_t write, EmbeddedVisionSystem *sys)
            : Desc(std::move(desc)), BytesToRead(read), BytesToWrite(write), System(sys) {}

    virtual ~NetCommand() = default;

    virtual int exec(void *) = 0;

    virtual std::unique_ptr<NetData> data() {
        return nullptr;
    };

    std::string desc() {
        return Desc;
    }

    std::size_t bytesToRead() {
        return BytesToRead;
    }

    std::size_t bytesToWrite() {
        return BytesToWrite;
    }

protected:
    std::string Desc;
    std::size_t BytesToRead;
    std::size_t BytesToWrite;
    EmbeddedVisionSystem *System;
};

class NetCommandList {
public:
    explicit NetCommandList(EmbeddedVisionSystem *sys);

    bool isValid(uint16_t type) const {
        return !(CmdList.find(type) == CmdList.end());
    }

    std::shared_ptr<NetCommand> command(uint16_t type) const {
        return CmdList.at(type);
    }

private:
    const std::map<uint16_t, std::shared_ptr<NetCommand>> CmdList;
};

}

#endif // NETCOMMAND_H
