//
// Created by jiangchao on 3/11/20.
//

#ifndef UNIXFILE_H
#define UNIXFILE_H

#include <string>

namespace EVS {

struct UnixFile {
    UnixFile(const char *FileName, int Flags);

    ~UnixFile();

    const std::string File;
    const int FileDescriptor;
};

}

#endif //UNIXFILE_H
