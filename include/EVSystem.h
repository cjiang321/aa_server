/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SYSTEM_H
#define SYSTEM_H

#include <cstring>
#include <memory>

#include "ImageSensor.h"
#include "FrameStore.h"

namespace EVS {

/**
 * @todo write docs
 */
struct SystemInfo {
    uint16_t processor[2];
    uint16_t fpga[2];
    uint16_t board[2];
    uint16_t date[6];
    uint32_t soft_ver[3];
    uint32_t firm_ver[3];
    uint32_t board_id[4];

    SystemInfo() {
        memset(this, 0, sizeof(SystemInfo));
    }

    ~SystemInfo() = default;

    enum processor_model {
        MCF5249 = 0x00000000,
        OMAP3503 = 0x00000001,
        OMAP3530 = 0x00010001,
        DM3730 = 0x00000002,
    };

    enum fpga_model {
        EP1C6T144I7N = 0x00000000,
        EP1C6T144C7N = 0x00010000,
        EP1C6T144C8N = 0x00020000,
        EP3C25F324I7N = 0x00000001,

    };

    enum board_model {
        FIREFLASH = 0x00000001,
        ARMFLASH_C1 = 0x00000000,
        ARMFLASH_C3 = 0x00010000,
        ARMFLASH_MINI = 0x00020000,
        ARMFLASH_PROTO = 0x00030000,
    };

    void setProcessor(processor_model model) {
        std::memcpy(processor, &model, sizeof(uint32_t));
    }

    void setFpga(fpga_model model) {
        std::memcpy(fpga, &model, sizeof(uint32_t));
    }

    void setBoard(board_model model) {
        std::memcpy(board, &model, sizeof(uint32_t));
    }

    void setDate(uint16_t year, uint16_t month = 0, uint16_t day = 0, uint16_t hour = 0, uint16_t minute = 0,
                 uint16_t second = 0) {
        date[0] = year;
        date[1] = month;
        date[2] = day;
        date[3] = hour;
        date[4] = minute;
        date[5] = second;
    }

    void setSoftVer(uint32_t major, uint32_t minor = 0, uint32_t rel = 0) {
        soft_ver[0] = major;
        soft_ver[1] = minor;
        soft_ver[2] = rel;
    }

    void setFirmVer(uint32_t major = 0, uint32_t minor = 0, uint32_t rel = 0) {
        firm_ver[0] = major;
        firm_ver[1] = minor;
        firm_ver[2] = rel;
    }

    void setBoardId(uint32_t id[4]) {
        std::memcpy(board_id, id, 4 * sizeof(uint32_t));
    }

} __attribute__((packed));

/*
 * represents components in the system
 */
struct EmbeddedVisionSystem {
    SystemInfo Info;
    std::vector<std::shared_ptr<FrameStore>> FrameStores;
    std::vector<std::shared_ptr<ImageSensor>> Sensors;
    std::size_t Selection = 0;

    explicit EmbeddedVisionSystem() = default;

    ~EmbeddedVisionSystem() = default;

    FrameStore *selectedFrameStore() {
        return FrameStores[Selection].get();
    }

    ImageSensor *selectedSensor() {
        return Sensors[Selection].get();
    }

    void selectSensor(std::size_t i) {
        selectedFrameStore()->disable();  // ensure current framestore has been disabled
        Selection = i;
    }
};

}

#endif // SYSTEMINFO_H
