/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTIL_H
#define UTIL_H

#include <cstdint>
#include <cassert>
#include <type_traits>

namespace EVS {

uint16_t swap16(uint16_t v);

/// ceil for integral division
template<typename T>
T divCeil(T a, T b) {
    static_assert(std::is_integral<T>::value, "integral required");
    assert(b != 0 && "divisior must be non-zero");
    return (a == 0) ? 0 : ((a - 1) / b + 1);
}

}

#endif // UTIL_H
