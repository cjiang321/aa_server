/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef D5M_H
#define D5M_H

#include "I2CSlave.h"
#include "ImageSensor.h"
#include "MMapModule.h"

namespace EVS {

/**
 * @todo write docs
 */
class D5M : public ImageSensor, public I2CSlave, public MMapModule {
public:
    explicit D5M(std::string name, uint32_t HWAddr);

    ~D5M() override ;

    int setConfig(SensorConfig &c) override;

    int setExposure(float time) override;

private:
    int rowBin();

    int columnBin();    // referred as WDC in datasheet
    int rowSkip();

    int columnSkip();

    float setExposureFromDevice();

    float rowTime();

    uint16_t readReg(uint8_t reg);

    void writeReg(uint8_t reg, uint16_t val);


    float PixelClkFreq;

    /// fpga register map
    enum FPGARegiterOffset {
        CSR = 0,
        Width = 0x4,
        Height = 0x8,
        Test = 0xc,
    };

    /// d5m read mod 2 bit settings
    enum D5MReadMode2 {
        MirrorRow = (1 << 15),
        MirrorColumn = (1 << 14),
        ShowDarkColumns = (1 << 12),
        ShowDarkRows = (1 << 11),
        RowBLC = (1 << 6),
        ColumnSum = (1 << 5),
    };

    /// d5m i2c register map
    enum SensorRegisterOffset {
        RowStart = 0x01,
        ColumnStart = 0x02,
        RowSize = 0x03,
        ColumnSize = 0x04,
        HorizontalBlank = 0x05,
        VerticalBlank = 0x06,
        OutputControl = 0x07,
        ShutterWidthUpper = 0x08,
        ShutterWidthLower = 0x09,
        PixelClockControl = 0x0A,
        Restart = 0x0B,
        ShutterDelay = 0x0C,
        ReadMode2 = 0x20,
        RowAddressMode = 0x22,
        ColumnAddressMode = 0x23,
    };
};

}

#endif // D5M_H

