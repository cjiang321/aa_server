/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef I2CSLAVE_H
#define I2CSLAVE_H

#include <vector>
#include <cstdint>
#include <cstring>
#include <stdexcept>

// C header for i2c operation
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>

namespace EVS {

/**
 * @todo write docs
 */
    class I2CSlave {
    public:
        I2CSlave(const I2CSlave &) = delete;

        I2CSlave &operator=(const I2CSlave &) = delete;

        /// default constructor
        /// \param bus i2c bus number
        /// \param addr 7-bit slave address
        explicit I2CSlave(int bus, uint16_t addr) : SlaveAddr(addr) {
            std::string FileName = "/dev/i2c-" + std::to_string(bus);
            FD = open(FileName.c_str(), O_RDWR);
            if (FD < 0)
                throw std::runtime_error("Failed to open " + FileName);

            if (ioctl(FD, I2C_SLAVE, addr) < 0)
                throw std::runtime_error("Failed to acquire bus access and/or talk to slave.");
        }

        /// default destructor
        virtual ~I2CSlave() {
            close(FD);
        }

        /// read data from i2c
        template<typename T>
        T read() {
            static_assert(std::is_integral<T>::value, "Integral required");
            T buf;
            if (read(FD, &buf, sizeof(T)))
                return buf;

            return (T) -1;
        }

        /// write data to i2c
        template<typename T>
        int write(T Val) {
            static_assert(std::is_integral<T>::value, "Integral required");
            if (write(FD, &Val, sizeof(T)))
                return 0;

            return -1;
        }

        enum I2COp {
            I2CWrite = 0,
            I2CRead = 1,    ///< equivalent to I2C_M_RD
        };

        /// perform register access to i2c device
        template<typename AT, typename DT, size_t AS = sizeof(AT), size_t DS = sizeof(DT)>
        int accessReg(AT offset, DT &Val, I2COp op) {
            static_assert(std::is_integral<DT>::value, "Integral required for data type");
            static_assert(std::is_integral<AT>::value, "Integral required for offset type");

            uint8_t buf[AS + DS];   /// write buffer
            uint16_t s = AS;        /// write buffer size

            memcpy(buf, &offset, AS);
            if (op == I2CWrite) {
                memcpy(buf + AS, &Val, DS);
                s += DS;
            }

            std::vector<i2c_msg> Messages{{SlaveAddr, (uint16_t) I2CWrite, s, buf}};

            /// first msg: write the register address,
            /// second msg: the data (read or write depending on @op)
            if (op == I2CRead)
                Messages.push_back({SlaveAddr, (uint16_t) op, sizeof(DT), (uint8_t *) &Val});

            struct i2c_rdwr_ioctl_data data = {
                    Messages.data(),
                    (uint32_t) Messages.size()
            };

            return ioctl(FD, I2C_RDWR, &data);
        }

    private:
        int FD;
        const uint16_t SlaveAddr;
    };

}

#endif // I2CSLAVE_H
