/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REQUEST_H
#define REQUEST_H

#include "ImageSensor.h"
#include "Reply.h"
#include "NetCommand.h"

namespace EVS {

// NOTE this structure contains 2 bytes padding after CmdType, this is to avoid
// alignment issue with accessing Exposure (4-byte float)
struct Request {
    struct CommandInfo {
        u16 CmdType;
        union CommandData {
            SensorConfig DevConfig;
            struct SumParam {
                u16 NumToSum;
                u16 Ctrl;
            } __attribute__((packed));
            float Exposure;
            u32 SensorSelection;
        } Data;
    } Info;
    std::shared_ptr<NetCommand> Cmd;
};

/// Network stream parser
///
/// Parse the stream against the given command list, form a request to be executed
class RequestParser {
public:
    enum Result {
        WAIT,   ///< more data needed
        DONE,   ///< a request is fully filled, ready to execute
        ERROR,  ///< something wrong happened during parse
    };

    explicit RequestParser(std::shared_ptr<NetCommandList> CL);

    Result parse(Request &r, asio::const_buffer b);

private:
    enum ParserState {
        WaitCommand,    ///< parser waiting for command header
        WaitData,       ///< parser waiting for additional data
    };

    ParserState State = WaitCommand;    ///< current parser state
    size_t BytesParsed = 0;             ///< current parsed length count in byte
    const std::shared_ptr<NetCommandList> CmdList;
};

/// Handler for request
///
/// executes command and create the reply object
class RequestHandler {
public:
    explicit RequestHandler() = default;

    static std::unique_ptr<Reply> handle(Request &r);
};

}

#endif // REQUEST_H
