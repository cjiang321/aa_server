/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MMAPMODULE_H
#define MMAPMODULE_H

#include "Types.h"

namespace EVS {

/**
 * @todo write docs
 */
class MMapModule {
public:
    /**
     * Default constructor
     *
     * @base: virtual base address for the module
     */
    explicit MMapModule(u32 Span, u32 HWBaseAddr);

    virtual ~MMapModule();

protected:
    u32 read(u32 addr) const;

    void write(u32 addr, u32 data);

    bool verifyModule(u32 reg, u32 id) const;

    const u32 MAGIC = 0x9654;

    u8 *MapBase;
    u32 MapSpan;
    u8 *ModuleBase;
};

}

#endif // MMAPMODULE_H
