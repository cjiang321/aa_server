/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NETDATA_H
#define NETDATA_H

#include <cstddef>
#include <vector>

namespace EVS {

///
/// Base class representing some data that needs to be sent over the network
/// can act as a scatter-gather list
///
class NetData {
public:
    enum DataType {
        IMAGE = 0,  ///< an image
        PROFILE,    ///< a profile
        GENERIC,    ///< arrays of pointer and size, for generic usage
    };

    explicit NetData(DataType t) : Type(t) {}

    virtual ~NetData() = default;

    DataType type() const {
        return Type;
    }

    virtual std::vector<const void *> data() = 0;

    virtual std::vector<std::size_t> sizes() = 0;

protected:
    DataType Type;
};

///
/// Generic data, the object don't own the actual data, just have pointers and size info
///
class GenericData : public NetData {
public:
    explicit GenericData(std::vector<const void *> p, std::vector<size_t> s)
            : NetData(GENERIC), PtrList(p), SizeList(s) {}

    std::vector<const void *> data() override {
        return PtrList;
    }

    std::vector<std::size_t> sizes() override {
        return SizeList;
    }

private:
    std::vector<const void *> PtrList;
    std::vector<size_t> SizeList;
};

///
/// Temporal data object, generated when executing command for returning to the client, the data should be only
/// used once and be freed upon sending to the client, one example of such object is the non-stored temperature value
///
template<typename T>
class TemporalData : public NetData {
public:
    explicit TemporalData(const T &data)
            : NetData(GENERIC), Data(data) {}

    std::vector<const void *> data() override {
        return {&Data};
    }

    std::vector<std::size_t> sizes() override {
        return {sizeof(T)};
    }

private:
    T Data;
};

}

#endif // NETDATA_H
