//
// Created by jiangchao on 3/10/20.
//

#ifndef TYPES_H
#define TYPES_H

namespace EVS {

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;

}

#endif //TYPES_H
