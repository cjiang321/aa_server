/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGESENSOR_H
#define IMAGESENSOR_H

#include <string>

#include "Types.h"

namespace EVS {

/// structure for setting image sensor over the network
struct SensorConfig {
    u16 control;    // Misc control parameters
    u16 gain;       // Gain
    u16 offset;     // Offset
    u16 lineIntegrationTime;    // line integration time
    u16 endY;                      // last line
    u16 frameIntegrationTime;           // Exposure time for frame intmode
    u16 startY;                     // first line
    u16 startX;                     // first column
    u16 endX;                       // last column
    u16 phase;                      // phase of the ADC
    u16 extra;               // configuration data that is not directly related to the imager.
} __attribute__((packed));

/// Image sensor class
/// the default implementation is a dummy device
class ImageSensor {
public:

    explicit ImageSensor(std::string name = "dummy") : Name(std::move(name)) {}

    virtual ~ImageSensor() = default;

    const std::string &name() const {
        return Name;
    }

    virtual int setExposure(float time) {
        Exposure = time;
        return 0;
    }

    virtual const float *exposure() const {
        return &Exposure;
    }

    virtual int setConfig(SensorConfig &c) {
        Config = c;
        return 0;
    }

    virtual const SensorConfig *config() const {
        return &Config;
    }

    virtual u32 detectorTemperature() { return 0; }

protected:
    SensorConfig Config = {0};
    float Exposure = 0;
    std::string Name;
};

}

#endif // SENSOR_H
