/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UARTSLAVE_H
#define UARTSLAVE_H

#include <asio.hpp>
#include <iomanip>
#include <iostream>

namespace EVS {
    class ByteString {
    public:
        explicit ByteString(const std::string s) : Str(s) {}

        friend std::ostream &operator<<(std::ostream &os, const ByteString &bs) {
            std::ios_base::fmtflags flags(os.flags());
            os << std::hex << std::setfill('0');
            for (const auto c : bs.Str) {
                os << "\\x" << std::setw(2) << static_cast<int>(c);
            }
            os.flags(flags);
            return os;
        }

    private:
        std::string Str;
    };

    class UARTSlave {
    public:
        explicit UARTSlave(const std::string &Dev) : Context(1), Port(Context, Dev) {
            Port.set_option(asio::serial_port::baud_rate(115200));
            Port.set_option(asio::serial_port::character_size()); // default to 8
            Port.set_option(asio::serial_port::stop_bits()); // default to 1
            Port.set_option(asio::serial_port::parity()); // default to none
            Port.set_option(asio::serial_port::flow_control()); // default to none
        }

        ~UARTSlave() {
            Port.close();
        }

        struct AsyncHandler {
            virtual void operator()(const asio::error_code &ec, std::size_t bytes_transferred) {}

            std::string Message;
        };

        void async_write(const std::string &msg, AsyncHandler *Handler) {
            Handler->Message = msg;
            asio::async_write(Port, asio::buffer(msg.data(), msg.size()), *Handler);
        }

        void async_read(AsyncHandler *Handler, std::size_t len = 1024) {
            assert(len > 0 && "read length should be greater than 0");
            Handler->Message = std::string(len, 0);
            asio::async_read(Port, asio::buffer(&Handler->Message[0], Handler->Message.size()), *Handler);
        }

        void async_run(int timeout = -1) {
            if (timeout < 0) {
                Context.run();
                return;
            }

            Context.run_for(std::chrono::minutes(timeout));
        }

        int write(const std::string &msg) {
            std::cout << "[UART] sending: " << ByteString(msg) << std::endl;
            return asio::write(Port, asio::buffer(msg.data(), msg.size()));
        }

        std::string read() {
            std::string data;
            std::size_t size = asio::read_until(Port, asio::dynamic_buffer(data), '\r');
            std::cout << "[UART] received: " << size << " characters" << std::endl;
            auto ret = data.substr(0, size);
            std::cout << "[UART] received: " << ByteString(ret) << std::endl;
            std::cout << "data (" << data.size() << "), ret (" << ret.size() << ")\n";
            return ret;
        }

    private:
        asio::io_context Context;
        asio::serial_port Port;
    };

};

#endif // UARTSLAVE_H
