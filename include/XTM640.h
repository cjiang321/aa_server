/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef XTM640_H
#define XTM640_H

#include "ImageSensor.h"
#include "MMapModule.h"
#include "UARTSlave.h"

namespace EVS {

/**
 * @todo write docs
 */
class XTM640 : public ImageSensor, public MMapModule, public UARTSlave {
public:
    explicit XTM640(std::string name, uint32_t HWAddr);

    ~XTM640() override;

    int setConfig(SensorConfig &c) override;

    int setExposure(float time) override;

    uint32_t detectorTemperature() override;

    uint32_t readReg(uint32_t addr);

    void writeReg(uint32_t addr, uint32_t val);

private:
    float PixelClkFreq;

    /// fpga register map
    enum FPGARegisterOffset {
        CSR = 0,
        Width = 0x4,
        Height = 0x8,
        Test = 0xc,
    };

    enum RegisterBaseAddress {
        CommonRegisterBase = 0x0,
        ApplicationRegisterBase = 0x1100,
        HistogramEqualizationRegisterBase = 0x1980,
    };

    enum CommonRegisterOffset {
        ID = 0x0,
        SerialNo = 0x4,
        CameraName = 0x8,
        XSPVersion = 0x28,
        FirmwareVersion = 0x2c,
    };

    enum ApplicationRegisterOffset {
        SysMode = 0x00,
        ExposureTime = 0xe4,
        ROIFlipping = 0x50,
        ROIXStart = 0x54,
        ROIYStart = 0x58,
        ROIXSize = 0x5c,
        ROIYSize = 0x60,
        Temperature = 0x9c,
    };

    enum HistogramEqualizationRegisterOffset {
        HistogramEqualizationConfig = 0x00,
        HistogramEqualizationOutliers = 0x04,
    };
};

}

#endif // D5M_H
