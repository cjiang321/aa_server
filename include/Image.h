/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGE_H
#define IMAGE_H

#include "Types.h"
#include "NetData.h"

namespace EVS {

/**
 * @todo write docs
 */
class Image : public NetData {
public:
    enum PixelSize {
        BPP8 = 1,
        BPP16 = 2,
    };

    struct ImageDescriptor {
        u16 FrameNo;    ///< Frame number (return value)
        u32 FrameTime;  ///< integration frame time (return value)
        u16 Width;      ///< Image size (call parameter)
        u16 Height;     ///< Image size (call parameter)
        u16 *Ptr;       ///< Image buffer (call parameter)
    } __attribute__((packed));

    explicit Image(u16 frame, u32 exp, u16 width, u16 height, PixelSize ps);

    explicit Image(u32 exp, u16 width, u16 height);

    ~Image() override;

    /**
     * map a image using mmap
     * @param fd file descriptor to /dev/mem
     * @param base_addr hardware address of the beginning of the buffer
     */
    bool map(u32 BaseAddr);

    /**
     * Assign a image pointer to the Image object, note when Image object is destructed, the memory will be freed
     * @param ptr pointer to the image data
     */
    void assign(u16 *ptr);

    bool hasData() const;

    std::vector<const void *> data() override;

    std::vector<std::size_t> sizes() override;

    const ImageDescriptor *desc() const;

private:
    ImageDescriptor Desc;
    std::size_t MapSize = 0;
    PixelSize BPP;
    bool IsMapped = false;
};

}

#endif // IMAGE_H
