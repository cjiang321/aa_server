/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EVSSERVER_H
#define EVSSERVER_H

#include <asio.hpp>
#include "EVSConnection.h"

namespace EVS {

/**
 * @todo write docs
 */
class Server {
public:
    Server(const Server &) = delete;

    Server &operator=(const Server &) = delete;

    /**
     * Default constructor
     */
    explicit Server(std::string host, unsigned short port, EmbeddedVisionSystem *sys);

    /**
     * Destructor
     */
    ~Server();

    void run();

private:
    void accept();

    void await_stop();

    asio::io_context IOContext;
    asio::signal_set Signals;
    std::shared_ptr<NetConnection> Connection;
    asio::ip::tcp::acceptor Acceptor;
    std::shared_ptr<NetCommandList> CmdList;
};

}

#endif // EVSSERVER_H
