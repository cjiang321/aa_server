/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EVSCONNECTION_H
#define EVSCONNECTION_H

#include "Request.h"
#include "Reply.h"

#include <array>

#define BUFFER_SIZE 8192

namespace EVS {

/**
 * @todo write docs
 */
class NetConnection : public std::enable_shared_from_this<NetConnection> {
public:
    NetConnection(const NetConnection &) = delete;

    NetConnection &operator=(const NetConnection &) = delete;

    /**
     * Default constructor
     */
    explicit NetConnection(asio::ip::tcp::socket socket, std::shared_ptr<NetCommandList> cmdList);

    /**
     * Destructor
     */
    ~NetConnection();

    void start();

    void stop();

private:
    void read();

    void write();

    void endiannessTest();

    asio::ip::tcp::socket Socket;
    std::array<unsigned char, BUFFER_SIZE> Buffer;
    RequestParser RP;
    Request Req;
    std::unique_ptr<Reply> Rply;

    static int Count;
};

}

#endif // EVSCONNECTION_H
