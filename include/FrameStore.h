/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FRAMESTORE_H
#define FRAMESTORE_H

#include "MMapModule.h"
#include "Image.h"

#include <memory>

namespace EVS {

class FrameStore {
public:
    FrameStore() = default;

    virtual ~FrameStore() = default;

    virtual std::unique_ptr<Image> getImage(Image::PixelSize ps) {
        auto image = std::make_unique<Image>(0, 0, Width, Height, ps);

        // create a dummy gradient image
        void *imageData = std::malloc(Width * Height * ps);
        for (int h = 0; h < Height; h++)
            for (int w = 0; w < Width; w++) {
                auto pt = static_cast<u16 *>(imageData);
                pt[h * Width + w] = h + w;
            }
        image->assign(static_cast<u16 *>(imageData));

        return image;
    }

    virtual void setImageSize(u16 w, u16 h) {
        Width = w;
        Height = h;
    }

    virtual void enable() {}

    virtual void disable() {}

protected:
    u16 Width = 0;
    u16 Height = 0;
};

/**
 * @todo write docs
 */
class EVSFrameStore : public MMapModule, public FrameStore {
public:
    explicit EVSFrameStore(u32 HWBaseAddr);

    void enable() override;

    void disable() override;

    void setImageSize(u16 w, u16 h) override;

    std::unique_ptr<Image> getImage(Image::PixelSize ps) override;

    u16 getVipHeight() const;

    u16 getVipWidth() const;

    bool isEnabled() const;

    bool isBpp16() const;

    bool isBusy() const;

    u16 getCurrentBufferIdx() const;

    u16 getCurrentFrameIdx() const;

    enum FrameStoreCtrl {
        CTL_DISABLE = 0,
        CTL_ENABLE = (1 << 0),
        CTL_BLOCK = (1 << 1),
        CTL_BPP16 = (1 << 2),
        CTL_CONT = (1 << 3),
    };

    void setCtrl(u32 ctrl);

    void setMaxBuffers(int num);

    void setMaxFrames(int num);

    void setBaseAddr(u32 addr);

    void setBufferStride(u32 stride);

private:
    u32 bufferAddress(int idx) const;

    u32 BufferBase = 0;
    u32 BufferCnt = 0;
    u32 BufferStride = 0;

    // module type id
    const u32 ID = 0x1;

    // register map
    enum RegisterOffset {
        REG_Type = 0x0,
        REG_Status = 0x10,
        REG_VideoInfo = 0x14,
        REG_CurrentBuffer = 0x18,
        REG_CurrentFrame = 0x1c,
        REG_Control = 0x20,
        REG_BaseAddr = 0x24,
        REG_Stride = 0x28,
        REG_MaxBufferCount = 0x2c,
        REG_MaxFrameCount = 0x30,
    };
};

}

#endif // FRAMESTORE_H
