/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REPLY_H
#define REPLY_H

#include <asio.hpp>
#include "NetData.h"

#include <vector>
#include <memory>

#include "Types.h"

namespace EVS {

/// an acknowledge packet for informing the client the status of command execution
struct Acknowledge {
    u16 Type;  ///< packet type
    u32 Error; ///< function error return code
} __attribute__((packed));

///
/// A class gathers NetData and convert them into asio::buffers, also adds acknowledge packet
/// contains everything ready to be sent over the network
/// the class will obtain ownership for NetData other than the generic ones
///
class Reply {
public:
    /**
     * Default constructor
     */
    Reply(u16 type, u32 err) : Ack({type, err}) {}

    /**
     * Destructor
     */
    ~Reply() = default;

    /// explicitly set the type and error number
    [[deprecated("use constructor directly instead")]]void setAck(u16 type, u32 err);

    /// directly set a pointer and corresponding size to create a asio::buffer, does not claim the ownership
    void setBuffer(void *p, size_t s);

    /// push a NetData into the reply and claiming ownership to the underlying memory
    void pushData(std::unique_ptr<NetData> d);

    std::vector<asio::const_buffer> buffers() const;

    void print();

private:
    Acknowledge Ack;
    std::vector<asio::const_buffer> Buffers;    ///< a series of asio::buffer which holds the data to be transferred
    std::vector<std::unique_ptr<NetData>> Data; ///< arrays for claiming ownership of data
};

}

#endif // REPLY_H
