/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "MMapModule.h"

#include <system_error>
#include <cerrno>
#include <iostream>

#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>

#include "UnixFile.h"

namespace EVS {

MMapModule::MMapModule(u32 Span, u32 HWBaseAddr) {
    auto devmem = UnixFile("/dev/mem", (O_RDWR | O_SYNC));

    // check if base addr and Span are page aligned
    const u32 PageSize = static_cast<u32>(getpagesize());
    const u32 LowBitsMask = PageSize - 1;
    const u32 HighBitsMask = static_cast<u32>(-1) - LowBitsMask;

    const u32 base = HWBaseAddr & HighBitsMask;
    MapSpan = (Span & HighBitsMask) + (Span & LowBitsMask) ? PageSize : 0;

    MapBase = static_cast<u8 *>(mmap(nullptr, MapSpan, PROT_READ | PROT_WRITE, MAP_SHARED, devmem.FileDescriptor,
                                     base));
    if (MapBase == MAP_FAILED) {
        throw std::system_error(errno, std::generic_category(), std::string("failed to map module"));
    }
    ModuleBase = MapBase + (HWBaseAddr & LowBitsMask);
    std::cout << "mapped " << reinterpret_cast<void *>(HWBaseAddr) << " @ "
              << static_cast<void *>(ModuleBase) << "\n";
}

MMapModule::~MMapModule() {
    if (MapBase && MapBase != MAP_FAILED)
        munmap(MapBase, MapSpan);
}

u32 MMapModule::read(u32 addr) const {
    return *((volatile u32 *) (ModuleBase + addr));
}

void MMapModule::write(u32 addr, u32 data) {
    *((volatile u32 *) (ModuleBase + addr)) = data;
}

bool MMapModule::verifyModule(u32 reg, u32 id) const {
    return (reg == ((MAGIC << 16u) + id));
}

}
