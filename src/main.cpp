#include <iostream>

#include "EVSystem.h"
#include "EVSServer.h"
#include "D5M.h"
#include "XTM640.h"

#define HPS2FPGA_LW_BASE 0xff200000

std::shared_ptr<EVS::EVSFrameStore> init_fs(uint32_t offset) {
    auto fs = std::make_shared<EVS::EVSFrameStore>(HPS2FPGA_LW_BASE + offset);
    fs->disable();
    fs->setBaseAddr(0x3c000000);
    fs->setMaxBuffers(2);
    fs->setBufferStride(0xc00000); // max 12MiB per image
    std::cout << "fs init completed\n";
    return fs;
}

int main(int argc, char **argv) {
    EVS::EmbeddedVisionSystem sys;
    sys.Info.setProcessor(EVS::SystemInfo::OMAP3503);
    sys.Info.setFpga(EVS::SystemInfo::EP3C25F324I7N);
    sys.Info.setBoard(EVS::SystemInfo::ARMFLASH_C1);
    sys.Info.setFirmVer(0, 1, 2);
    sys.Info.setSoftVer(0, 2, 3);
    sys.Info.setDate(2018, 5, 15);

    auto UseDummy = false;
    if (argc > 1 && !strcmp(argv[1], "--use-dummy")) {
        UseDummy = true;
    }

    try {
        if (UseDummy) {
            std::cout << "[DEV] initialise dummy devices...\n";
            sys.FrameStores.emplace_back(std::make_shared<EVS::FrameStore>());
            sys.Sensors.emplace_back(std::make_shared<EVS::ImageSensor>());
        } else {
            std::cout << "[DEV] initialize D5M...\n";
            sys.FrameStores.emplace_back(init_fs(0x0));
            sys.Sensors.emplace_back(std::make_shared<EVS::D5M>("D5M", HPS2FPGA_LW_BASE + 0x1000));

            std::cout << "[DEV] initialize XTM640...\n";
            sys.FrameStores.emplace_back(init_fs(0x100));
            sys.Sensors.emplace_back(std::make_shared<EVS::XTM640>("XTM640",HPS2FPGA_LW_BASE + 0x1100));
        }

        std::cout << "[NET] starting server...\n";
        EVS::Server s("", 2222, &sys);
        s.run();
    }
    catch (std::exception &e) {
        std::cerr << "exception: " << e.what() << std::endl;
    }

    return 0;
}
