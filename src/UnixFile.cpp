//
// Created by jiangchao on 3/11/20.
//

#include "UnixFile.h"

#include <system_error>
#include <iostream>
#include <unistd.h>
#include <fcntl.h>

namespace EVS {

UnixFile::UnixFile(const char *FileName, int Flags) : File(FileName), FileDescriptor(open(FileName, Flags)) {
    if (FileDescriptor < 0)
        throw std::system_error(errno, std::generic_category(), std::string("could not open ") + File);
}

UnixFile::~UnixFile() {
    if (close(FileDescriptor)) {
        std::error_code ec(errno, std::generic_category());
        std::cerr << "failed to close " << File << " (" << ec.message() << ")\n";
    }
    // NOTE error from close is not handled
}

}