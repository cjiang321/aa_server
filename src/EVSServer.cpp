/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "EVSServer.h"

namespace EVS {

Server::Server(std::string host, unsigned short port, EmbeddedVisionSystem *sys)
        : IOContext(1),
          Signals(IOContext),
          Acceptor(IOContext),
          CmdList(std::make_shared<NetCommandList>(sys)) {
    Signals.add(SIGINT);
    Signals.add(SIGTERM);

    await_stop();

    asio::ip::tcp::endpoint endpoint;
    if (host.size() > 0) {
        endpoint = asio::ip::tcp::endpoint(asio::ip::address::from_string(host), port);
    } else {
        endpoint = asio::ip::tcp::endpoint(asio::ip::tcp::v4(), port);
    }

    Acceptor.open(endpoint.protocol());
    Acceptor.set_option(asio::ip::tcp::acceptor::reuse_address(true));
    Acceptor.bind(endpoint);
    Acceptor.listen();

    accept();
}

Server::~Server() = default;

void Server::accept() {
    Acceptor.async_accept([this](std::error_code err, asio::ip::tcp::socket socket) {
        if (!Acceptor.is_open()) {
            return;
        }

        if (!err) {
            // start a connection here
            Connection = std::make_shared<NetConnection>(std::move(socket), CmdList);
            Connection->start();
        }

        accept();
    });
}

void Server::run() {
    IOContext.run();
}

void Server::await_stop() {
    Signals.async_wait([this](std::error_code, int) {
        Acceptor.close();
        if (Connection)
            Connection->stop();
    });
}

}
