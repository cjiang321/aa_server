/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Util.h"

#include <limits>

namespace EVS {

    template<typename T>
    inline T rotl(T v, unsigned int b) {
        static_assert(std::is_integral<T>::value, "rotate of non-integral type");
        static_assert(!std::is_signed<T>::value, "rotate of signed type");
        constexpr unsigned int num_bits{std::numeric_limits<T>::digits};
        static_assert(0 == (num_bits & (num_bits - 1)), "rotate value bit length not power of two");
        constexpr unsigned int count_mask{num_bits - 1};
        const unsigned int mb{
                b & count_mask
        };
        using promoted_type = typename std::common_type<int, T>::type;
        using unsigned_promoted_type = typename std::make_unsigned<promoted_type>::type;
        return ((unsigned_promoted_type{v} << mb)
                | (unsigned_promoted_type{v} >> (-mb & count_mask)));
    }

    uint16_t swap16(uint16_t v) {
        return rotl<uint16_t>(v, 8);
    }

}
