#include "Image.h"

#include <stdexcept>
#include <iostream>

#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>

#include "UnixFile.h"

namespace EVS {

Image::Image(u16 frame, u32 exp, u16 width, u16 height, Image::PixelSize ps)
        : NetData(IMAGE), Desc({frame, exp, width, height, nullptr}), BPP(ps) {
}

Image::Image(u32 exp, u16 width, u16 height)
        : NetData(IMAGE), Desc({0, exp, width, height, nullptr}), BPP(BPP16) {
}

Image::~Image() {
    if (Desc.Ptr && Desc.Ptr != MAP_FAILED) {
        if (IsMapped) {
            munmap(Desc.Ptr, MapSize);
            return;
        }
        free(Desc.Ptr);
    }
}

bool Image::map(u32 BaseAddr) {
    auto devmem = UnixFile("/dev/mem", (O_RDWR | O_SYNC));
    auto PageSize = getpagesize();

    MapSize = (static_cast<std::size_t>(Desc.Width) * Desc.Height * BPP + PageSize - 1) / PageSize * PageSize;
    std::cout << "mapping image of size " << Desc.Width * Desc.Height * BPP
              << "(" << MapSize << ") @ "
              << reinterpret_cast<void *>(BaseAddr) << "...\n";
    Desc.Ptr = static_cast<u16 *>(mmap(nullptr, MapSize, PROT_READ | PROT_WRITE, MAP_SHARED, devmem.FileDescriptor,
                                       BaseAddr));
    if (Desc.Ptr == MAP_FAILED) {
        // TODO consider to implement exception
        //throw std::system_error(errno, std::generic_category(), "failed to map image");
        return false;
    }

    IsMapped = true;
    return true;
}

bool Image::hasData() const {
    return Desc.Ptr != nullptr;
}

const Image::ImageDescriptor *Image::desc() const {
    return &Desc;
}

std::vector<const void *> Image::data() {
    std::vector<const void *> res;
    res.push_back(&Desc);
    if (hasData())
        res.push_back(Desc.Ptr);

    return res;
}

std::vector<std::size_t> Image::sizes() {
    std::vector<size_t> res;
    res.push_back(sizeof(Desc));
    if (hasData())
        res.push_back(Desc.Width * Desc.Height * BPP);

    return res;
}

void Image::assign(u16 *ptr) {
    Desc.Ptr = ptr;
}

}
