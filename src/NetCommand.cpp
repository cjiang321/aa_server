/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "NetCommand.h"
#include "Request.h"
#include <iostream>

namespace EVS {

class CmdGetSysInfo : public NetCommand {
public:
    explicit CmdGetSysInfo(EmbeddedVisionSystem *sys)
            : NetCommand("get system info", 2, 70, sys) {}

    int exec(void *) override {
        std::cout << "executing cmd " << desc() << "\n";
        return 0;
    }

    std::unique_ptr<NetData> data() override {
        std::vector<const void *> dv = {&System->Info};
        std::vector<size_t> ds = {sizeof(System->Info)};
        return std::make_unique<GenericData>(dv, ds);
    }
};

class CmdGetDevConfig : public NetCommand {
public:
    explicit CmdGetDevConfig(EmbeddedVisionSystem *sys)
            : NetCommand("get dev config", 2, 28, sys) {}

    int exec(void *) override {
        std::cout << "executing cmd " << desc() << "\n";
        return 0;
    }

    std::unique_ptr<NetData> data() override {
        ImageSensor *Sensor = System->selectedSensor();
        auto config = Sensor->config();
        std::vector<const void *> dv = {config};
        std::vector<size_t> ds = {sizeof(*config)};
        return std::make_unique<GenericData>(dv, ds);
    }
};

class CmdSetDevConfig : public NetCommand {
public:
    explicit CmdSetDevConfig(EmbeddedVisionSystem *sys)
            : NetCommand("set dev config", 24, 6, sys) {}

    int exec(void *arg) override {
        std::cout << "executing cmd " << desc() << "\n";
        auto config = (Request::CommandInfo::CommandData *) arg;
        System->selectedSensor()->setConfig(config->DevConfig);
        System->selectedFrameStore()->setImageSize(config->DevConfig.endX - config->DevConfig.startX,
                                                   config->DevConfig.endY - config->DevConfig.startY);
        return 0;
    }
};

class CmdGetExposure : public NetCommand {
public:
    explicit CmdGetExposure(EmbeddedVisionSystem *sys)
            : NetCommand("get exposure time", 2, 14, sys) {}

    int exec(void *) override {
        std::cout << "executing cmd " << desc() << "\n";
        return 0;
    }

    std::unique_ptr<NetData> data() override {
        ImageSensor *Sensor = System->selectedSensor();
        auto config = Sensor->config();
        auto exp = Sensor->exposure();
        return std::make_unique<Image>((uint32_t) (*exp * 6.0e7),
                                       config->endX - config->startX,
                                       config->endY - config->startY);
    }
};

class CmdSetExposure : public NetCommand {
public:
    explicit CmdSetExposure(EmbeddedVisionSystem *sys)
            : NetCommand("set exposure time", 6, 32, sys) {}

    int exec(void *arg) override {
        std::cout << "executing cmd " << desc() << "\n";
        auto exp = (Request::CommandInfo::CommandData *) arg;
        System->selectedSensor()->setExposure(exp->Exposure);
        return 0;
    }

    std::unique_ptr<NetData> data() override {
        ImageSensor *Sensor = System->selectedSensor();
        auto config = Sensor->config();
        auto exp = Sensor->exposure();
        std::vector<const void *> dv = {config, exp};
        std::vector<size_t> ds = {sizeof(*config), sizeof(*exp)};
        return std::make_unique<GenericData>(dv, ds);
    }
};

class CmdGetImage8Bpp : public NetCommand {
public:
    explicit CmdGetImage8Bpp(EmbeddedVisionSystem *sys) : NetCommand("get 8bpp image", 2, 20, sys) {}

    int exec(void *) override {
        std::cout << "executing cmd " << desc() << "\n";
        Img = System->selectedFrameStore()->getImage(Image::BPP8);
        if (Img)
            return 0;

        return 1;
    }

    std::unique_ptr<NetData> data() override {
        return std::move(Img);
    }

private:
    std::unique_ptr<Image> Img;
};

class CmdGetImage16Bpp : public NetCommand {
public:
    explicit CmdGetImage16Bpp(EmbeddedVisionSystem *sys) : NetCommand("get 16bpp image", 2, 20, sys) {}

    int exec(void *) override {
        std::cout << "executing cmd " << desc() << "\n";
        Img = System->selectedFrameStore()->getImage(Image::BPP16);
        if (Img)
            return 0;

        return 1;
    }

    std::unique_ptr<NetData> data() override {
        return std::move(Img);
    }

private:
    std::unique_ptr<Image> Img;
};

class CmdSumImage : public NetCommand {
public:
    explicit CmdSumImage(EmbeddedVisionSystem *sys) : NetCommand("sum image", 6, 0, sys) {}

    int exec(void *arg) override {
        std::cout << "executing cmd " << desc() << "\n";
        return 0;
    }
};

class CmdGetTemperature : public NetCommand {
public:
    explicit CmdGetTemperature(EmbeddedVisionSystem *sys) : NetCommand("get temperature", 2, 4, sys) {}

    int exec(void *arg) override {
        std::cout << "executing cmd " << desc() << "\n";
        return 0;
    }

    std::unique_ptr<NetData> data() override {
        ImageSensor *Sensor = System->selectedSensor();
        auto tmp = Sensor->detectorTemperature();
        std::cout << "got temperature: " << Sensor->name() << " " << tmp << "cK\n";
        return std::make_unique<TemporalData<uint32_t>>(tmp);
    }
};

class CmdSelectSensor : public NetCommand {
public:
    explicit CmdSelectSensor(EmbeddedVisionSystem *sys) : NetCommand("select sensor", 6, 2, sys) {}

    int exec(void *arg) override {
        std::cout << "executing cmd " << desc() << "\n";
        auto config = (Request::CommandInfo::CommandData *) arg;
        std::cout << "selecting " << config->SensorSelection << " ...\n";
        System->selectSensor(config->SensorSelection);
        std::cout << "set sensor to " << System->selectedSensor()->name() << "\n";
        return 0;
    }
};

NetCommandList::NetCommandList(EmbeddedVisionSystem *sys)
        : CmdList({
                          {0xaa20, std::make_shared<CmdGetSysInfo>(sys)},
                          {0xaa21, std::make_shared<CmdGetDevConfig>(sys)},
                          {0xaa22, std::make_shared<CmdSetDevConfig>(sys)},
                          {0xaa23, std::make_shared<CmdGetExposure>(sys)},
                          {0xaa24, std::make_shared<CmdSetExposure>(sys)},
                          {0xaa30, std::make_shared<CmdGetImage16Bpp>(sys)},
                          {0xaa31, std::make_shared<CmdGetImage8Bpp>(sys)},
                          {0xaa33, std::make_shared<CmdSumImage>(sys)},
                          {0xaa42, std::make_shared<CmdGetTemperature>(sys)},
                          {0xaa60, std::make_shared<CmdSelectSensor>(sys)},
                  }) {}

}
