/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "XTM640.h"
#include "Util.h"

#include <iostream>
#include <asio.hpp>

namespace EVS {

XTM640::XTM640(std::string name, uint32_t HWAddr)
        : ImageSensor(std::move(name)),
          MMapModule(0x40, HWAddr),
          UARTSlave("/dev/ttyS1") {
    // test sensor
    auto id = readReg(CommonRegisterBase + ID);
    if (id >> 16u != 0x1b21) {
        std::stringstream ss;
        ss << "invalid camera vid/pid: " << std::hex << id;
        throw std::runtime_error(ss.str());
    }

    Config.startX = 0;
    Config.startY = 0;
    Config.endX = Config.startX + 640;
    Config.endY = Config.startY + 480;

    printf("startx: %d\n", Config.startX);
    printf("starty: %d\n", Config.startY);
    printf("endx: %d\n", Config.endX);
    printf("endy: %d\n", Config.endY);

    PixelClkFreq = 16000000;
    Exposure = 0;
    printf("current exposure time: %f sec\n", Exposure);

    auto Val = readReg(ApplicationRegisterBase + SysMode);
    Val |= 0x5u; // set manual offset and gain
    writeReg(ApplicationRegisterBase + SysMode, Val);

    printf("start configuring fpga module ...\n");
    // disable FPGA module
    MMapModule::write(CSR, 0);
    // set width and height to FPGA
    MMapModule::write(Width, Config.endX - Config.startX);
    printf("written width %d\n", Config.endX - Config.startX);
    MMapModule::write(Height, Config.endY - Config.startY);
    printf("written height %d\n", Config.endY - Config.startY);

    printf("read back width: %d\n", MMapModule::read(Width));
    printf("read back height: %d\n", MMapModule::read(Height));

    // re-enable FPGA module
    MMapModule::write(CSR, 1);
    if (MMapModule::read(CSR))
        printf("xtm640 fpga init completed\n");
    else
        printf("failed to re-enable d5m fpga\n");
}

XTM640::~XTM640() = default;

//FIXME: do nothing for now
int XTM640::setConfig(SensorConfig &c) {
    return 0;
}

//FIXME: do nothing now, preserve the default exp time
int XTM640::setExposure(float time) {
    return 0;
}

uint32_t XTM640::readReg(uint32_t addr) {
    std::cout << "read from 0x" << std::hex << addr << std::dec << std::endl;
    std::stringstream msg;
    msg << "#h" << std::hex << addr << "?\r";
    auto len = UARTSlave::write(msg.str());
    std::cout << "written " << len << " characters\n";
    auto ret = UARTSlave::read();
    if (ret.substr(0, 4) == "NACK")
        return 0;

    ret = ret.substr(1, ret.size() - 2);
    return std::stoul(ret, nullptr, 16);
}

void XTM640::writeReg(uint32_t addr, uint32_t val) {
    std::cout << "write 0x" << std::hex << val << " to 0x" << addr << std::dec << std::endl;
    std::stringstream msg;
    msg << "#h" << std::hex << addr << "=h" << val << "\r";
    auto len = UARTSlave::write(msg.str());
    std::cout << "written " << len << " characters\n";
    auto ret = UARTSlave::read();
    assert(ret == "ACK\r");
}

uint32_t XTM640::detectorTemperature() {
    return readReg(ApplicationRegisterBase + Temperature);
}

}
