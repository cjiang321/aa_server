/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "EVSConnection.h"

#include <iostream>

namespace EVS {

int NetConnection::Count = 0;

NetConnection::NetConnection(asio::ip::tcp::socket socket, std::shared_ptr<NetCommandList> cmdList)
        : Socket(std::move(socket)), RP(std::move(cmdList)) {
    Count++;
    printf("creating new connection ... total active: %d\n", Count);
}


NetConnection::~NetConnection() {
    Count--;
    printf("destroyed connection, %d remained\n", Count);
}

void NetConnection::endiannessTest() {
    auto self(shared_from_this());
    uint16_t data = 0x0CA0;
    asio::async_write(Socket, asio::buffer(&data, sizeof(data)), [self](std::error_code ec, std::size_t) {
        if (!ec) {
        }
    });
}

void NetConnection::start() {
    endiannessTest();
    read();
}

void NetConnection::stop() {
    Socket.close();
}

void NetConnection::read() {
    auto self(shared_from_this());

    Socket.async_read_some(asio::buffer(Buffer), [this, self](std::error_code err, std::size_t bytes_read) {
        if (!err) {
            auto res = RP.parse(Req, asio::buffer(Buffer, bytes_read));
            if (res == RequestParser::DONE) {
                //handle the request here
                Rply = RequestHandler::handle(Req);
                this->write();
            } else if (res == RequestParser::ERROR) {
                // handle wrong command here
            }

            read();
        } else {
            std::cout << "error reading from connection (" << err.value() << ": " << err.message() << ")\n";
            if (err.value() == asio::error::eof)
                this->stop();
        }
    });
}

void NetConnection::write() {
    Rply->print();

    for (auto &buf : Rply->buffers()) {
        std::cout << "@" << buf.data() << ", size: " << buf.size() << "\n";
    }

    auto self(shared_from_this());
    asio::async_write(Socket, Rply->buffers(), [self](std::error_code err, std::size_t bytes_written) {
        if (!err) {
            std::cout << "written " << bytes_written << " bytes to network\n";
        } else {
            std::cout << "error writing to connection (" << err.value() << ")\n";
        }
    });
}

}
