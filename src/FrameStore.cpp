/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "FrameStore.h"

#include <cassert>
#include <stdexcept>
#include <chrono>
#include <iostream>

namespace EVS {

EVSFrameStore::EVSFrameStore(u32 HWBaseAddr)
        : MMapModule(0x40, HWBaseAddr) {
    u32 magic = read(REG_Type);
    printf("got module type&magic: %x\n", magic);
    if (!verifyModule(magic, ID)) {
        throw std::runtime_error("failed to detect EVSFrameStore");
    }
}

bool EVSFrameStore::isEnabled() const {
    u32 stat = read(REG_Status);
    printf("fs stat: %x\n", stat);
    return (stat & (1 << 0)) != 0;
}

bool EVSFrameStore::isBpp16() const {
    u32 stat = read(REG_Status);
    printf("fs stat: %x\n", stat);
    return (stat & (1 << 1)) != 0;
}

bool EVSFrameStore::isBusy() const {
    u32 stat = read(REG_Status);
    printf("fs stat: %x\n", stat);
    return (stat & (1 << 2)) != 0;
}

u16 EVSFrameStore::getCurrentBufferIdx() const {
    return static_cast<u16>(read(REG_CurrentBuffer));
}

u16 EVSFrameStore::getCurrentFrameIdx() const {
    return static_cast<u16>(read(REG_CurrentFrame));
}

u16 EVSFrameStore::getVipHeight() const {
    return static_cast<u16>((read(REG_VideoInfo) & 0xffff));
}

u16 EVSFrameStore::getVipWidth() const {
    return static_cast<u16>(((read(REG_VideoInfo) >> 16) & 0xffff));
}

void EVSFrameStore::setBaseAddr(u32 addr) {
    write(REG_BaseAddr, addr);
    BufferBase = addr;
}

void EVSFrameStore::setBufferStride(u32 stride) {
    write(REG_Stride, stride);
    BufferStride = stride;
}

void EVSFrameStore::setMaxBuffers(int num) {
    write(REG_MaxBufferCount, num);
    BufferCnt = num;
}

void EVSFrameStore::setMaxFrames(int num) {
    write(REG_MaxFrameCount, num);
}

void EVSFrameStore::setCtrl(u32 ctrl) {
    printf("set EVSFrameStore ctrl: %x\n", ctrl);
    write(REG_Control, ctrl);
}

u32 EVSFrameStore::bufferAddress(int idx) const {
    assert(static_cast<u32>(idx) < BufferCnt && "buffer index out of bounds");
    return BufferBase + idx * BufferStride;
}

// ask frame store to store 1 image and return the object reference
std::unique_ptr<Image> EVSFrameStore::getImage(Image::PixelSize ps) {
    std::cout << "starting to acquire a image " << ps << " bytes per pixel\n";
    setMaxFrames(1);
    auto flags = CTL_ENABLE | (ps == Image::PixelSize::BPP16 ? CTL_BPP16 : 0u);
    setCtrl(flags);

    assert(isEnabled() && "failed to enable frame store");

    auto start = std::chrono::steady_clock::now();
    // NOTE max frame is hard coded to 1
    while (getCurrentFrameIdx() < 1) {
        if (std::chrono::steady_clock::now() - start >= std::chrono::seconds(5)) {
            printf("current idx: %d\n", getCurrentFrameIdx());
            throw std::runtime_error("timeout waiting frame store");
        }
    }

    std::cout << "acquisition completed\n";
    std::cout << "frame: " << getCurrentFrameIdx() << "\n";
    std::cout << "width: " << Width << "\n";
    std::cout << "height: " << Height << "\n";

    //TODO get integration time from some where
    auto image = std::make_unique<Image>(getCurrentFrameIdx(), 0.1, Width, Height, ps);

    if (image->map(bufferAddress(0)))
        return image;
    return nullptr;
}

void EVSFrameStore::setImageSize(u16 w, u16 h) {
    if (w == 0 || h == 0) {
        Width = getVipWidth();
        Height = getVipHeight();
        return;
    }
    FrameStore::setImageSize(w, h);
}

void EVSFrameStore::enable() {
    setCtrl(CTL_ENABLE);
}

void EVSFrameStore::disable() {
    setCtrl(CTL_DISABLE);
}

}