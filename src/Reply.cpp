/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Reply.h"

#include <iostream>

namespace EVS {

void Reply::setAck(u16 type, u32 err) {
    Ack.Error = err;
    Ack.Type = type;
    setBuffer(&Ack, sizeof(Ack));
}

void Reply::setBuffer(void *p, size_t s) {
    Buffers.emplace_back(asio::buffer(p, s));
}

void Reply::pushData(std::unique_ptr<NetData> d) {
    // setup asio buffer
    auto DataVec = d->data();
    auto SizeVec = d->sizes();
    assert(DataVec.size() == SizeVec.size() &&
           "data pointer vector size is not the same as size vector size for NetData");

    for (size_t i = 0, N = SizeVec.size(); i < N; i++)
        Buffers.emplace_back(DataVec[i], SizeVec[i]);

    // claiming ownership of the data
    Data.push_back(std::move(d));
}

std::vector<asio::const_buffer> Reply::buffers() const {
    return Buffers;
}

void Reply::print() {
    std::cout << "[replay]\n";
    std::cout << "ack: " << reinterpret_cast<void *>(Ack.Type) << ", " << Ack.Error << "\n";
    std::cout << "number of data packet: " << Data.size() << "\n";
    for (const auto &d : Data) {
        std::cout << "type " << d->type() << "\n";
        for (const auto &dv : d->data()) {
            std::cout << dv << " ";
        }
        std::cout << "\n";
        for (const auto &sv : d->sizes()) {
            std::cout << sv << " ";
        }
        std::cout << "\n";
    }
}

}
