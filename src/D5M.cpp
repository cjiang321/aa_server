/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "D5M.h"
#include "Util.h"

#include <iostream>
#include <utility>

namespace EVS {

D5M::D5M(std::string name, uint32_t HWAddr)
        : ImageSensor(std::move(name)),
          EVS::I2CSlave(1, 0x5d),
          MMapModule(0x40, HWAddr) {
    Config.startX = readReg(ColumnStart);
    Config.startY = readReg(RowStart);
    Config.endX = Config.startX + readReg(ColumnSize) + 1;
    Config.endY = Config.startY + readReg(RowSize) + 1;

    printf("startx: %d\n", Config.startX);
    printf("starty: %d\n", Config.startY);
    printf("endx: %d\n", Config.endX);
    printf("endy: %d\n", Config.endY);

    PixelClkFreq = 25000000.0f;
    Exposure = setExposureFromDevice();
    printf("current exposure time: %f sec\n", Exposure);

    // flip the image in both direction
    auto val = readReg(ReadMode2);
    val |= (MirrorRow | MirrorColumn);
    writeReg(ReadMode2, val);

    printf("start configuring fpga module ...\n");
    // disable FPGA module
    MMapModule::write(CSR, 0);
    // set width and height to FPGA
    MMapModule::write(Width, Config.endX - Config.startX);
    printf("written width %d\n", Config.endX - Config.startX);
    MMapModule::write(Height, Config.endY - Config.startY);
    printf("written height %d\n", Config.endY - Config.startY);

    printf("read back width: %d\n", MMapModule::read(Width));
    printf("read back height: %d\n", MMapModule::read(Height));

    // re-enalbe FPGA module
    MMapModule::write(CSR, 1);
    if (MMapModule::read(CSR))
        printf("d5m fpga init completed\n");
    else
        printf("failed to re-enable d5m fpga\n");
}

D5M::~D5M() = default;

uint16_t D5M::readReg(uint8_t reg) {
    uint16_t val;
    accessReg<uint8_t, uint16_t>(reg, val, I2CRead);

    return EVS::swap16(val);
}

void D5M::writeReg(uint8_t reg, uint16_t val) {
    auto v = EVS::swap16(val);
    accessReg<uint8_t, uint16_t>(reg, v, I2CWrite);
}

int D5M::setConfig(SensorConfig &c) {
    std::cout << "goint to set sensor windows to\n";
    std::cout << "start [" << c.startX << ", " << c.startY << "]\n";
    std::cout << "end   [" << c.endX << ", " << c.endY << "]\n";

    // disable FPGA module
    MMapModule::write(CSR, 0);

    writeReg(ColumnStart, c.startX);
    writeReg(ColumnSize, (c.endX - c.startX) / 2 * 2 + 1);
    writeReg(RowStart, c.startY);
    writeReg(RowSize, (c.endY - c.startY) / 2 * 2 + 1);

    // read back settings
    Config.startX = readReg(ColumnStart);
    Config.endX = readReg(ColumnSize) + Config.startX + 1;
    Config.startY = readReg(RowStart);
    Config.endY = readReg(RowSize) + Config.startY + 1;
    Config.control = c.control;

    std::cout << "sensor windows set to\n";
    std::cout << "start [" << Config.startX << ", " << Config.startY << "]\n";
    std::cout << "end   [" << Config.endX << ", " << Config.endY << "]\n";

    //TODO validate value?

    //set width and height to FPGA
    MMapModule::write(Width, Config.endX - Config.startX);
    MMapModule::write(Height, Config.endY - Config.startY);

    MMapModule::write(Test, Config.control & (1u << 7));

    // re-enalbe FPGA module
    MMapModule::write(CSR, 1);
    if (MMapModule::read(CSR))
        printf("re-enalbed d5m fpga\n");
    else
        printf("failed to re-enable d5m fpga\n");

    return 0;
}

//FIXME: do nothing now, preserve the default exp time
int D5M::setExposure(float time) {
    return 0;
}

/// From datasheet:
/// exp = sw * t_row - so * 2 * t_pixelclk
///
/// SW = max(1, (2*16 × Shutter_Width_Upper) + Shutter_Width_Lower)
/// SO = 208 × (Row_Bin + 1) + 98 + min(SD, SDmax) – 94
/// SD = Shutter_Delay + 1
/// SDmax = 1232; if SW < 3
///         1504, else
float D5M::setExposureFromDevice() {
    auto SD = readReg(ShutterDelay) + 1;
    auto SW = std::max(1, (2 * 16 * readReg(ShutterWidthUpper) + readReg(ShutterWidthLower)));
    auto SDMax = (SW < 3) ? 1232 : 1504;
    auto SO = 208 * (rowBin() + 1) + 98 + std::min(SD, SDMax) - 94;

    return SW * rowTime() - SO * 2 * 1.0f / PixelClkFreq;
}

/// Equation in the datasheet says
/// HBMIN = 208 * (Row_bin + 1) + 64 + (WDC / 2)
/// however this is contradictory with the table 1.6 later
/// form the following equation from the table
/// HBMIn = 208 * (row_bin + 1) + 64 + 40 / (WDC + 1)
float D5M::rowTime() {
    auto HBMIN = 208 * (rowBin() + 1) + 64 + 40 / (columnBin() + 1);  ///< minimum horizontal blanking
    auto W = 2 * EVS::divCeil((readReg(ColumnSize) + 1), (2 * (columnSkip() + 1))); ///< effective image width

    return 2 * 1.0f / PixelClkFreq *
           std::max(W / 2 + std::max(readReg(HorizontalBlank) + 1, HBMIN), 41 + 208 * (rowBin() + 1) + 99);
}

int D5M::rowBin() {
    auto m = readReg(RowAddressMode);
    return (m >> 4) & 0x3;
}

int D5M::columnBin() {
    auto m = readReg(ColumnAddressMode);
    return (m >> 4) & 0x3;
}

int D5M::rowSkip() {
    auto m = readReg(RowAddressMode);
    return m & 0x7;
}

int D5M::columnSkip() {
    auto m = readReg(ColumnAddressMode);
    return m & 0x7;
}

}
