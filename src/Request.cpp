/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <chao.jiang-7@postgrad.manchester.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Request.h"

#include <iostream>

#define DEBUG_NET

namespace EVS {

RequestParser::RequestParser(std::shared_ptr<NetCommandList> CL) : CmdList(std::move(CL)) {
}

RequestParser::Result RequestParser::parse(Request &r, asio::const_buffer b) {
#ifdef DEBUG_NET
    auto pt0 = static_cast<const u8 *>(b.data());
    std::cout << "[buffer]";
    for (size_t i = 0; i < sizeof(r.Info); i++)
        std::cout << reinterpret_cast<void *>(*(pt0 + i)) << " ";
    std::cout << "\n";
#endif

    if (State == WaitCommand) {
        auto CmdBuffer = asio::buffer(&r.Info.CmdType, sizeof(r.Info.CmdType));
        auto copied = asio::buffer_copy(CmdBuffer + BytesParsed, b);
        b += copied;
        BytesParsed += copied;

        if (BytesParsed < 2)
            return WAIT;

        if (CmdList->isValid(r.Info.CmdType)) {
            r.Cmd = CmdList->command(r.Info.CmdType);
            State = WaitData;
        } else {
            return ERROR;
        }
    }

    auto DataBuffer = asio::buffer(&r.Info.Data, sizeof(r.Info.Data));
    BytesParsed += asio::buffer_copy(DataBuffer + (BytesParsed - 2), b);

    std::cout << "got cmd " << r.Cmd->desc() << ", need "
              << r.Cmd->bytesToRead() << " bytes in total, have " << BytesParsed << " now\n";

    if (r.Cmd->bytesToRead() > BytesParsed) {
        return WAIT;
    }

    BytesParsed = 0; // reset counter for next request
    if (State == WaitData)
        State = WaitCommand;
    return DONE;
}

std::unique_ptr<Reply> RequestHandler::handle(Request &req) {
    // execute the request
    auto ret = req.Cmd->exec(&req.Info.Data);
    // get the data pointer and size
    auto data = req.Cmd->data();

    // from the result construct a acknowledgement packet
    auto reply = std::make_unique<Reply>(req.Info.CmdType, static_cast<unsigned int>(ret));

    // pack everything into the reply object, its content will be sent
    if (data)
        reply->pushData(std::move(data));

    return reply;
}

}
