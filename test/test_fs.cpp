
#include <iostream>
#include <cassert>

#include "FrameStore.h"
#include <unistd.h>
#include <fcntl.h>

#define HPS2FPGA_LW_BASE 0xff200000

int main(int argc, char** argv)
{
    int fd = open("/dev/mem", (O_RDWR | O_SYNC));
    if (fd == -1) {
        printf("ERROR: could not open \"/dev/mem\"\n");
        return -1;
    }

    try {

        auto fs = new FrameStore(fd, HPS2FPGA_LW_BASE + 0x0);
        fs->setCtrl(FrameStore::DISABLE);
        fs->setBaseAddr(0x1c000000);
        fs->setMaxBuffers(2);
        fs->setBufferStride(0x400000); // max 4MiB per image
        std::cout << "fs init completed\n";

        for (int i = 0; i < 4; i++) {
        fs->setCtrl(FrameStore::ENABLE);
        assert(fs->isEnable() && "failed to enable fs");
        fs->setCtrl(FrameStore::DISABLE);
        sleep(1);
        }

    } catch (std::exception& e) {
        std::cerr << e.what() << "\n";
    }

    close(fd);
    return 0;
}

