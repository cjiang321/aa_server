

#include <iostream>
#include <cassert>
#include "D5M.h"

#define HPS2FPGA_LW_BASE 0xff200000

int main(int argc, char** argv)
{
    int fd = open("/dev/mem", (O_RDWR | O_SYNC));
    if (fd == -1) {
        printf("ERROR: could not open \"/dev/mem\"\n");
        return -1;
    }

    try {
        D5M cam(fd, HPS2FPGA_LW_BASE + 0x1000);
    } catch (std::exception& e) {
        std::cerr << e.what() << "\n";
    }

    return 0;
}
