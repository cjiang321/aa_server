
#include <iostream>

#include "I2CSlave.h"
#include <cassert>


int main(int argc, char** argv)
{
    try {
        uint8_t reg = 0;
        uint16_t val;
        EVS::I2CSlave d5m(1, 0x5d);

        if (argc >= 2) {
            reg = (uint8_t)std::stoul(argv[1], 0, 16);

            if (argc > 2) {
                val = (uint16_t)std::stoul(argv[2], 0, 16);
                assert((d5m.accessReg<uint8_t, uint16_t>(reg, val, EVS::I2CSlave::I2CWrite) >= 0));
                std::cout << "written " << std::hex << val << " to 0x" << +reg << "\n";
            }

            assert((d5m.accessReg<uint8_t, uint16_t>(reg, val, EVS::I2CSlave::I2CRead) >= 0));
            std::cout << "@0x" << std::hex << +reg << " : " << val << "\n";
        }

    } catch (std::exception& e) {
        std::cerr << e.what() << "\n";
    }

    return 0;
}
