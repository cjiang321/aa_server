#include <iostream>

#include <asio.hpp>
#include <string>
#include <stdexcept>

#define MAX_LENGTH 1024

int main(int argc, char** argv)
{
    if (argc != 3) {
        std::cerr << "Usage: client <address> <port>\n";
        return 1;
    }

    try {
        asio::io_context io_context;

        asio::ip::tcp::socket s(io_context);
        asio::ip::tcp::resolver resolver(io_context);

        asio::connect(s, resolver.resolve(argv[1], argv[2]));

        char user[10];
        // consume the endianness-check half-word
        asio::read(s, asio::buffer(user, 2));
        if (static_cast<unsigned char>(user[0]) == 0xa0 && static_cast<unsigned char>(user[1]) == 0x0c)
            std::cout << "communication in little endian\n";
        else
            std::cout << "communication in big endian\n";

        std::cout << "cmd? ";
        std::cin.getline(user, 10);
        uint16_t cmd = std::stoul(user, nullptr, 16);
        printf("got %s [%x]\n", user, cmd);
        asio::write(s, asio::buffer(&cmd, 2));

        std::cout << "cmd sent\n";

        unsigned char data[100];
        auto len = 0;
        for (auto i = 0; i < 100; i++) {
            std::cout << "additional data byte? ";
            std::cin.getline(user, 10);
            if (user[0] == 0) {
                len = i;
                break;
            }
            data[i] = std::stoul(user);
        }

        if (len > 0) {
            std::cout << "sending " << len << " bytes additional data\n";
            asio::write(s, asio::buffer(data, len));
        } else {
            std::cout << "no additional data\n";
        }

        std::cout << "bytes to get? ";
        std::cin.getline(user, 10);
        len = std::stoul(user, nullptr, 10);

        unsigned char reply[len];
        auto reply_length = asio::read(s, asio::buffer(reply, len));
        for (size_t i = 0; i < reply_length; i++) {
            std::cout << i << ": " << std::hex << static_cast<int>(reply[i]) << "\n";
        }
    }
    catch (std::exception& e) {
        std::cerr << "Exception: " << e.what() << "\n";
    }
}
